'use strict'
const request = require('request')
var browser = request.defaults({
  headers: {'User-Agent': 'The Botfather NodeJS module (https://www.npmjs.com/package/palitanx-api)'}
})

module.exports = class Palitanx {
    constructor() {
    	this.apiurl = 'https://api.palitanx.com/v1/';
    }

    getMarkets(callback) {
    	browser(this.apiurl + 'public/getmarkets', { json: true }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                callback(false, body)
            } else {
                callback(true, "")
            }
    	})
    }

    getCurrencies(callback) {
    	browser(this.apiurl + 'public/getcurrencies', { json: true }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                callback(false, body)
            } else {
                callback(true, "")
            }
    	})
    }

    getTicker(market, callback) {
    	browser(this.apiurl + 'public/getticker/' + market, { json: true }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                callback(false, body)
            } else {
                callback(true, "")
            }
    	})
    }

    getMarketSummaries(callback) {
    	browser(this.apiurl + 'public/getmarketsummaries', { json: true }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                callback(false, body)
            } else {
                callback(true, "")
            }
    	})
    }

    getMarketSummary(market, callback) {
        browser(this.apiurl + 'public/getmarketsummary/' + market, { json: true }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                callback(false, body)
            } else {
                callback(true, "")
            }
        })
    }

    getOrderBook(market, type, callback) {
        browser(this.apiurl + 'public/getorderbook/' + market + '/' + type, { json: true }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                callback(false, body)
            } else {
                callback(true, "")
            }
        })
    }

    getMarketHistory(market, callback) {
        browser(this.apiurl + 'public/getmarkethistory/' + market, { json: true }, function(error, res, body) {
            if (!error && res.statusCode == 200) {
                callback(false, body)
            } else {
                callback(true, "")
            }
        })
    }
}